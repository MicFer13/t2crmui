import {PROSPECT_LIST_REQUEST, PROSPECT_LIST_SUCCESS, PROSPECT_LIST_FAIL} from '../constants/UserConstants'

const prospectInitialState = {
    pending: false,
    prospects: [],
    error: null
}

function prospectsListReducer(state=prospectInitialState,action) {
    switch (action.type) {
        case PROSPECT_LIST_REQUEST:
            return {...state, pending: true};
        case PROSPECT_LIST_SUCCESS:
            return {...state, pending: false, prospects: action.payload};
        case PROSPECT_LIST_FAIL:
            return {...state, pending: false, error: action.payload};
        default:
            return state;
    }
}

export default prospectsListReducer;