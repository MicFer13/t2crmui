import { combineReducers } from 'redux'
import AddUserReducer from "./AddUserReducer";
import GetUserReducer from "./GetUserReducer";
import GetUserHistoryReducer from "./GetUserHistoryReducer";
import LeadListReducer from './LeadsListReducer'
import ProspectListReducer from './ProspectsListReducer'
import CustomerListReducer from './CustomersListReducer'
import UpdateUserReducer from "./UpdateUserReducer";

export default combineReducers({
    AddUserReducer,
    GetUserReducer,
    GetUserHistoryReducer,
    LeadListReducer,
    CustomerListReducer,
    ProspectListReducer,
    UpdateUserReducer
})