import {GET_USER_REQUEST, GET_USER_SUCCESS, GET_USER_FAIL} from "../constants/UserConstants"

const getUserInitialState = {
    user: {},
    error: "",
    loading: false,
};

function GetUserReducer(state = getUserInitialState, action) {
    switch (action.type) {
        case GET_USER_REQUEST:
            return { ...state, loading: false };
        case GET_USER_SUCCESS:
            return { ...state, loading: true, user: action.payload };
        case GET_USER_FAIL:
            return { ...state, loading: false, error: action.payload };
        default:
            return state;
    }
}

export default GetUserReducer;