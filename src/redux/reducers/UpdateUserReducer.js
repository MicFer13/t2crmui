import {
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAIL,
    UPDATE_USER_RESET
} from '../constants/UserConstants'

const updateUserInitialState = {
    user: {},
    error: "",
    updated: false,
};

function UpdateUserReducer(state = updateUserInitialState,action) {
    switch (action.type) {
        case UPDATE_USER_REQUEST:
            return { ...state, updated: false };
        case UPDATE_USER_SUCCESS:
            return { ...state, updated: true, user: action.payload };
        case UPDATE_USER_FAIL:
            return { ...state, updated: false, error: action.payload };
        case UPDATE_USER_RESET:
            return {...state, updated: updateUserInitialState.updated, user: updateUserInitialState.user, error: updateUserInitialState};
        default:
            return state;
    }
}

export default UpdateUserReducer;