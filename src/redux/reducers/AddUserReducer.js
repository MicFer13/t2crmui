import {
    ADD_USER_REQUEST,
    ADD_USER_SUCCESS,
    ADD_USER_FAIL,
    ADD_USER_RESET,
    SET_FIRSTNAME,
    SET_SURNAME,
    SET_GENDER,
    SET_DOB,
    SET_EMAIL_ADDRESS,
    SET_ADDRESS,
    SET_ZIP_CODE,
    SET_PHONE_NUMBER,
    SET_PREFERRED_METHOD
} from "../constants/UserConstants"

const initialState = {
    user: {
        firstname: "",
        surname: "",
        gender: "Please Select",
        date_of_birth: "",
        email_address: "",
        address: "",
        zipcode: "",
        phone_number: "",
        method_of_contact: "Please Select"
    },
    error: "",
    pending: false,
    submitted: false
}

function addUserReducer(state = initialState, action) {
    switch (action.type) {
        case SET_FIRSTNAME:
            return {...state, user: { ...state.user, firstname: action.value} };
        case SET_SURNAME:
            return {...state, user: { ...state.user, surname: action.value} };
        case SET_GENDER:
            return {...state, user: { ...state.user, gender: action.value} };
        case SET_DOB:
            return {...state, user: { ...state.user, date_of_birth: action.value} };
        case SET_EMAIL_ADDRESS:
            return {...state, user: { ...state.user, email_address: action.value} };
        case SET_ADDRESS:
            return {...state, user: { ...state.user, address: action.value} };
        case SET_ZIP_CODE:
            return {...state, user: { ...state.user, zipcode: action.value} };
        case SET_PHONE_NUMBER:
            return {...state, user: { ...state.user, phone_number: action.value} };
        case SET_PREFERRED_METHOD:
            return {...state, user: { ...state.user, method_of_contact: action.value} };
        case ADD_USER_REQUEST:
            return {...state, pending: true, user: action.payload};
        case ADD_USER_SUCCESS:
            return {...state, pending: false, submitted: true, user: action.payload};
        case ADD_USER_FAIL:
            return {...state, pending: false, submitted: false, error: action.payload};
        case ADD_USER_RESET:
            return {...state, pending: initialState.pending, submitted: initialState.submitted, user: initialState.user, error: initialState.error};
        default:
            return state;
    }
}

export default addUserReducer;