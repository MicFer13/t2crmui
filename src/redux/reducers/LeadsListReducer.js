import {LEAD_LIST_REQUEST, LEAD_LIST_SUCCESS, LEAD_LIST_FAIL} from '../constants/UserConstants'

const leadsInitialState = {
    pending: false,
    leads: [],
    error: null
}

function leadsListReducer(state=leadsInitialState,action) {
    switch (action.type) {
        case LEAD_LIST_REQUEST:
            return {...state, pending: true};
        case LEAD_LIST_SUCCESS:
            return {...state, pending: false, leads: action.payload};
        case LEAD_LIST_FAIL:
            return {...state, pending: false, error: action.payload};
        default:
            return state;
    }
}

export default leadsListReducer;