import {GET_USER_HISTORY_REQUEST, GET_USER_HISTORY_SUCCESS, GET_USER_HISTORY_FAIL} from "../constants/UserConstants"

const getUserHistoryInitialState = {
    history: [],
    error: "",
    loading: false,
};

function GetUserHistoryReducer(state = getUserHistoryInitialState, action) {
    switch (action.type) {
        case GET_USER_HISTORY_REQUEST:
            return { ...state, loading: false };
        case GET_USER_HISTORY_SUCCESS:
            return { ...state, loading: true, history: action.payload };
        case GET_USER_HISTORY_FAIL:
            return { ...state, loading: false, error: action.payload };
        default:
            return state;
    }
}

export default GetUserHistoryReducer;