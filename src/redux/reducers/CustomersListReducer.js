import {CUSTOMER_LIST_REQUEST, CUSTOMER_LIST_SUCCESS, CUSTOMER_LIST_FAIL} from '../constants/UserConstants'

const customerInitialState = {
    pending: false,
    customers: [],
    error: null
}

function customersListReducer(state=customerInitialState,action) {
    switch (action.type) {
        case CUSTOMER_LIST_REQUEST:
            return {...state, pending: true};
        case CUSTOMER_LIST_SUCCESS:
            return {...state, pending: false, customers: action.payload};
        case CUSTOMER_LIST_FAIL:
            return {...state, pending: false, error: action.payload};
        default:
            return state;
    }
}

export default customersListReducer;