import { CUSTOMER_LIST_REQUEST, CUSTOMER_LIST_SUCCESS, CUSTOMER_LIST_FAIL } from '../constants/UserConstants';
import UserService from "../../services/UserService";

const customerList = () => async (dispatch) => {
    try {
        dispatch({ type: CUSTOMER_LIST_REQUEST});

        const customerList = await UserService.getUsers('customer');

        dispatch({ type: CUSTOMER_LIST_SUCCESS, payload: customerList.data });

    } catch (error) {
        dispatch({ type: CUSTOMER_LIST_FAIL, payload: error.response.status });
    }
};

export default customerList;