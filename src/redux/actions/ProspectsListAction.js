import { PROSPECT_LIST_REQUEST, PROSPECT_LIST_SUCCESS, PROSPECT_LIST_FAIL } from '../constants/UserConstants';
import UserService from "../../services/UserService";

const prospectList = () => async (dispatch) => {
    try {
        dispatch({ type: PROSPECT_LIST_REQUEST});

        const prospectList = await UserService.getUsers('prospect');

        dispatch({ type: PROSPECT_LIST_SUCCESS, payload: prospectList.data });

    } catch (error) {
        dispatch({ type: PROSPECT_LIST_FAIL, payload: error.response.status });
    }
};

export default prospectList;