import { ADD_USER_REQUEST, ADD_USER_SUCCESS, ADD_USER_FAIL, ADD_USER_RESET } from "../constants/UserConstants"
import UserService from "../../services/UserService";

const addUser = (user) => async (dispatch) => {
    try {
        dispatch({ type: ADD_USER_REQUEST, payload: user });

        const newUser = await UserService.create(user)

        dispatch({ type: ADD_USER_SUCCESS, payload: newUser})

        dispatch({ type: ADD_USER_RESET})
    } catch (error) {
        dispatch({ type: ADD_USER_FAIL, payload: error });
    }
};

export default addUser;