import {GET_USER_REQUEST, GET_USER_SUCCESS, GET_USER_FAIL} from "../constants/UserConstants"
import UserService from "../../services/UserService";

const getUser = (id) => async (dispatch) => {
    try {
        dispatch({ type: GET_USER_REQUEST });

        const user = await UserService.get(id)

        dispatch({ type: GET_USER_SUCCESS, payload: user.data})
    } catch (error) {
        dispatch({ type: GET_USER_FAIL, payload: error });
    }
};

export default getUser;