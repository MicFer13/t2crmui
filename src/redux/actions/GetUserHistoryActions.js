import {GET_USER_HISTORY_REQUEST, GET_USER_HISTORY_SUCCESS, GET_USER_HISTORY_FAIL} from "../constants/UserConstants"
import UserService from "../../services/UserService";

const getHistory = (id) => async (dispatch) => {
    try {
        dispatch({ type: GET_USER_HISTORY_REQUEST });

        const history = await UserService.getHistory(id)

        dispatch({ type: GET_USER_HISTORY_SUCCESS, payload: history.data})
    } catch (error) {
        dispatch({ type: GET_USER_HISTORY_FAIL, payload: error });
    }
};

export default getHistory;