import { LEAD_LIST_REQUEST, LEAD_LIST_SUCCESS, LEAD_LIST_FAIL } from '../constants/UserConstants';
import UserService from "../../services/UserService";

const leadList = () => async (dispatch) => {
    try {
        dispatch({ type: LEAD_LIST_REQUEST});

        const leadList = await UserService.getUsers('lead');

        dispatch({ type: LEAD_LIST_SUCCESS, payload: leadList.data });

    } catch (error) {
        dispatch({ type: LEAD_LIST_FAIL, payload: error.response.status });
    }
};

export default leadList;