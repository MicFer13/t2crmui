import {UPDATE_USER_REQUEST, UPDATE_USER_SUCCESS, UPDATE_USER_FAIL, UPDATE_USER_RESET} from "../constants/UserConstants"
import UserService from "../../services/UserService";

const updateUser = (user) => async (dispatch) => {
    try {
        dispatch({ type: UPDATE_USER_REQUEST, payload: user });

        const updateUser = await UserService.updateUser(user)

        dispatch({ type: UPDATE_USER_SUCCESS, payload: updateUser})

        dispatch({ type: UPDATE_USER_RESET})
    } catch (error) {
        dispatch({ type: UPDATE_USER_FAIL, payload: error });
    }
};

export default updateUser;