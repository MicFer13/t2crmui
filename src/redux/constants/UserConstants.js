export const SET_FIRSTNAME = "SET_FIRSTNAME";
export const SET_SURNAME = "SET_SURNAME";
export const SET_GENDER = "SET_GENDER";
export const SET_DOB = "SET_DOB";
export const SET_EMAIL_ADDRESS = "SET_EMAIL_ADDRESS";
export const SET_ADDRESS = "SET_ADDRESS";
export const SET_ZIP_CODE = "SET_ZIP_CODE";
export const SET_PHONE_NUMBER = "SET_PHONE_NUMBER";
export const SET_PREFERRED_METHOD = "SET_PREFERRED_METHOD";
export const ADD_USER_REQUEST = "ADD_USER_REQUEST";
export const ADD_USER_SUCCESS = "ADD_USER_SUCCESS";
export const ADD_USER_FAIL = "ADD_USER_FAIL";
export const ADD_USER_RESET = "ADD_USER_RESET";
export const LEAD_LIST_REQUEST = "LEAD_LIST_REQUEST";
export const LEAD_LIST_SUCCESS = "LEAD_LIST_SUCCESS";
export const LEAD_LIST_FAIL = "LEAD_LIST_FAIL";
export const GET_USER_REQUEST = "GET_USER_REQUEST";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAIL = "GET_USER_FAIL";
export const GET_USER_HISTORY_REQUEST = "GET_USER_HISTORY_REQUEST";
export const GET_USER_HISTORY_SUCCESS = "GET_USER_HISTORY_SUCCESS";
export const GET_USER_HISTORY_FAIL = "GET_USER_HISTORY_FAIL";
export const UPDATE_USER_REQUEST = "UPDATE_USER_REQUEST";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";
export const UPDATE_USER_FAIL = "UPDATE_USER_FAIL";
export const UPDATE_USER_RESET = "UPDATE_USER_RESET";
export const CUSTOMER_LIST_REQUEST = "CUSTOMER_LIST_REQUEST";
export const CUSTOMER_LIST_SUCCESS = "CUSTOMER_LIST_SUCCESS";
export const CUSTOMER_LIST_FAIL = "CUSTOMER_LIST_FAIL";
export const PROSPECT_LIST_REQUEST = "PROSPECT_LIST_REQUEST";
export const PROSPECT_LIST_SUCCESS = "PROSPECT_LIST_SUCCESS";
export const PROSPECT_LIST_FAIL = "PROSPECT_LIST_FAIL";