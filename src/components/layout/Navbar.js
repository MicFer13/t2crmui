import React from 'react';
import {Navbar, Nav} from "react-bootstrap";
import { withRouter } from "react-router";

const WithRouterNavBar = (props) => {
    const { location } = props;
    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="/">Allstate CRM</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav activeKey={location.pathname} className="mr-auto">
                    <Nav.Link href="/leads">Leads</Nav.Link>
                    <Nav.Link href="/prospects">Prospects</Nav.Link>
                    <Nav.Link href="/customers">Customers</Nav.Link>
                    <Nav.Link href="/adduser">Add User</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};
const NavBar = withRouter(WithRouterNavBar);

export default NavBar;
