import React from 'react';
import {Link} from "react-router-dom";

const Home = () => {
    return (
        <section className="home">
            <div className="dark-overlay">
                <div className="home-inner">
                    <h1>Allstate CRM</h1>
                    <p>You're In Good Hands</p>
                    <div className="row d-none d-md-block">
                        <Link to={'/leads'} className="btn btn-info">Leads</Link>
                        <Link to={'/prospects'} className="btn btn-info mr-3 ml-3">Prospects</Link>
                        <Link to={'/customers'} className="btn btn-info">Customers</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Home;