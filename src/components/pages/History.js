import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import getHistory from "../../redux/actions/GetUserHistoryActions";
import {Link, Redirect} from "react-router-dom";
import getUser from "../../redux/actions/GetUserActions";

const History = (props) => {
    const userId = props.match.params.id;
    const dispatch = useDispatch()
    const state = useSelector((state) => state);

    const {history} = state.GetUserHistoryReducer
    const {user} = state.GetUserReducer

    useEffect(() => {
        if (userId) {
            dispatch(getHistory(userId));
            dispatch(getUser(userId));
        }
        return () => {
        };
    }, []);

    const url = "/" + user.status + "s"

    console.log("HISTORY >>> ", history)

    if (history.length !== 0) {
        return (
            <Fragment>
                <div className="row">
                    <div className="col-sm col-md-9">
                        <h2>History for {user.firstname} {user.surname}</h2>
                    </div>
                    <div className="d-none d-md-block col-md-3">
                        <Link className="btn btn-primary float-right" to={url}>Back</Link>
                    </div>
                </div>
                <hr/>
                {history.map((element, index) => {
                    return (
                        <ul key={index}>
                            <li><b>Log Entry {index + 1}:</b> {element.actions}</li>
                        </ul>
                    )
                })}
            </Fragment>
        );
    } else
        return null
};

export default History;
