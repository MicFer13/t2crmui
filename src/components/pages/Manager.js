import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import getUser from "../../redux/actions/GetUserActions";
import AddUser from "../users/AddUser";
import UpdateUser from "../users/UpdateUser";

const Manager = (props) => {
    const userId = props.match.params.id;
    const dispatch = useDispatch()
    const state = useSelector((state) => state);

    const {user} = state.GetUserReducer

    useEffect(() => {
        if (userId) {
            dispatch(getUser(userId));
        }
        return () => {};
    }, []);

    if (userId != user.id) {
        return (
            <AddUser />
        );
    } else {
    return (
        <UpdateUser user={user} />
    );
    }
};

export default Manager;
