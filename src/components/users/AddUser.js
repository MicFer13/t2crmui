import React from 'react';
import addUser from '../../redux/actions/AddUserActions';
import {connect, useDispatch} from 'react-redux'
import {Redirect} from "react-router-dom";

const ConnectedAddUser = (props) => {
    const dispatch = useDispatch()
    const {user} = props

    const setOnChange = (type, value) => {
        return dispatch({type: 'SET_' + type, value})
    }

    const handleChange = (e) => {
        setOnChange([e.target.name], e.target.value)
    }

    const onSubmit = (e) => {
        e.preventDefault();
        dispatch(addUser(user));
    }

    if (props.submitted) {
        return <Redirect to="/"/>
    } else
        return (
            <div>
                <h2>Add New User</h2>
                <form onSubmit={onSubmit}>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="addUserFirstName">First Name</label>
                            <input type="text" className="form-control" value={user.firstname} id="addUserFirstName"
                                   name="FIRSTNAME" onChange={handleChange} maxLength="20" required/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="addUserSurname">Last Name</label>
                            <input type="text" className="form-control" id="addUserSurname" value={user.surname}
                                   name="SURNAME" onChange={handleChange} maxLength="30" required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="addUserGender">Select Gender</label>
                            <select className="form-control" id="addUserGender" value={user.gender} name="GENDER"
                                    onChange={handleChange} required>
                                <option disabled>Please Select</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="addUserDateOfBirth">Date Of Birth</label>
                            <input type="date" className="form-control" id="addUserDateOfBirth"
                                   value={user.date_of_birth}
                                   name="DOB" onChange={handleChange} required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="addUserEmailAddress">Email address</label>
                            <input type="email" className="form-control" id="addUserEmailAddress"
                                   value={user.email_address}
                                   name="EMAIL_ADDRESS" onChange={handleChange} required/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="addUserAddress">Address</label>
                            <input type="text" className="form-control" id="addUserAddress" value={user.address}
                                   name="ADDRESS" onChange={handleChange} required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="addUserZipCode">Zip Code</label>
                            <input type="text" className="form-control" id="addUserZipCode" value={user.zipcode}
                                   name="ZIP_CODE" onChange={handleChange} maxLength="9" required/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="addUserContactNumber">Contact Number</label>
                            <input type="number" className="form-control" id="addUserContactNumber"
                                   value={user.phone_number}
                                   name="PHONE_NUMBER" onChange={handleChange} required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="addUserPreferredMethod">How you hear about us?</label>
                            <select className="form-control" id="addUserPreferredMethod" value={user.method_of_contact}
                                    name="PREFERRED_METHOD" onChange={handleChange} required>
                                <option disabled>Please Select</option>
                                <option>Email</option>
                                <option>Advert</option>
                                <option>Phone Call</option>
                                <option>Newspaper</option>
                                <option>Conference</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group">
                        <input type="submit" className="form-control btn btn-primary col-sm col-md-3" value="Add User"/>
                    </div>
                </form>
            </div>
        )
}

const mapStateToProps = (state) => {
    return {
        user: state.AddUserReducer.user,
        submitted: state.AddUserReducer.submitted,
        pending: state.AddUserReducer.pending,
        error: state.AddUserReducer.error
    }
}
const AddUser = connect(mapStateToProps)(ConnectedAddUser)

export default AddUser;