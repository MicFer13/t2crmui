import React, {useState, useEffect, Fragment} from 'react';
import PropTypes from "prop-types";
import {connect, useDispatch} from "react-redux";
import updateUser from "../../redux/actions/UpdateUserActions";
import {Redirect} from "react-router-dom";

const ConnectedUpdateUser = (props) => {
    const dispatch = useDispatch();
    const [user, setUser] = useState(props.user)

    useEffect(
        () => {
            setUser(props.user)
        },
        [props]
    )

    const handleChange = (e) => {
        const {name, value} = e.target;
        setUser({...user, [name]: value});
    };

    const handleChecked = (e) => {
        const {name, value} = e.target;
        let boolValue;
        if (value === 'false') {
            boolValue = true;
        } else if (value === 'true') {
            boolValue = false;
        }
        setUser({...user, [name]: boolValue});
    };

    const onSubmit = (e) => {
        e.preventDefault();
        dispatch(updateUser(user));
    }

    if (props.updated) {
        switch (user.status) {
            case 'lead':
                return <Redirect to="/leads"/>
            case 'prospect':
                return <Redirect to="/prospects"/>
            case 'customer':
                return <Redirect to="/customers"/>
            default:
                return <Redirect to="/"/>
        }
    } else
        return (
            <Fragment>
                <h2>{user.status.toUpperCase()} - {user.firstname} {user.surname}</h2>
                <hr/>
                <form onSubmit={onSubmit}>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserFirstName">First Name</label>
                            <input type="text" className="form-control" value={user.firstname} id="updateUserFirstName"
                                   name="firstname" disabled/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserSurname">Last Name</label>
                            <input type="text" className="form-control" id="updateUserSurname" value={user.surname}
                                   name="surname" disabled/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserGender">Select Gender</label>
                            <input type="text" className="form-control" id="updateUserGender" value={user.gender}
                                   name="gender" disabled/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserDateOfBirth">Date Of Birth</label>
                            <input type="text" className="form-control" id="updateUserDateOfBirth"
                                   value={user.date_of_birth}
                                   name="date_of_birth" disabled/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserEmailAddress">Email address</label>
                            <input type="email" className="form-control" id="updateUserEmailAddress"
                                   value={user.email_address}
                                   name="email_address" onChange={handleChange} required/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserAddress">Address</label>
                            <input type="text" className="form-control" id="updateUserAddress" value={user.address}
                                   name="address" onChange={handleChange} required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserZipCode">Zip Code</label>
                            <input type="text" className="form-control" id="updateUserZipCode" value={user.zipcode}
                                   name="zipcode" onChange={handleChange} maxLength="9" required/>
                        </div>
                        <div className="form-group col-sm">
                            <label htmlFor="updateUserContactNumber">Contact Number</label>
                            <input type="number" className="form-control" id="updateUserContactNumber"
                                   value={user.phone_number}
                                   name="phone_number" onChange={handleChange} required/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm col-md-6">
                            <label htmlFor="updateUserStatus">User Status</label>
                            <select className="form-control" id="updateUserStatus" value={user.status}
                                    name="status" onChange={handleChange} required>
                                <option value="lead">Lead</option>
                                <option value="prospect">Prospect</option>
                                <option value="customer">Customer</option>
                            </select>
                        </div>
                        {user.status === 'prospect' ?
                            (
                                <div className="form-group col-sm col-md-6">
                                    <label htmlFor="updateUserProduct">Select Product</label>
                                    <select className="form-control" id="updateUserProduct" name="product_selected"
                                        value={user.product_selected} onChange={handleChange}>
                                        <option/>
                                        <option>Boat Insurance</option>
                                        <option>Car Insurance</option>
                                        <option>Home Insurance</option>
                                        <option>Life Insurance</option>
                                    </select>
                                </div>
                            )
                            : null
                        }

                    </div>
                    {user.status === 'lead' ?
                        <div className="row">
                            <div className="form-group col-sm">
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" id="emailSent" name="email_sent"
                                           value={user.email_sent} checked={user.email_sent} onChange={handleChecked}/>
                                    <label className="form-check-label" htmlFor="emailSent">Email Sent</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" id="emailReturned"
                                           name="email_returned"
                                           value={user.email_returned} checked={user.email_returned}
                                           onChange={handleChecked}/>
                                    <label className="form-check-label" htmlFor="emailReturned">Email Returned</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" id="callMade"
                                           name="phone_call_made"
                                           value={user.phone_call_made} checked={user.phone_call_made}
                                           onChange={handleChecked}/>
                                    <label className="form-check-label" htmlFor="callMade">Call Made</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" id="callReturned"
                                           name="phone_call_returned"
                                           value={user.phone_call_returned} checked={user.phone_call_returned}
                                           onChange={handleChecked}/>
                                    <label className="form-check-label" htmlFor="callReturned">Call Returned</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input" type="checkbox" id="active" name="active"
                                           value={user.active} checked={user.active} onChange={handleChecked}/>
                                    <label className="form-check-label" htmlFor="active">Active</label>
                                </div>
                            </div>
                        </div>
                        : null
                    }
                    {user.status === 'prospect' ?
                        (
                            <div className="row">
                                <div className="form-group col-sm">
                                    <div className="form-check form-check-inline">
                                        <input className="form-check-input" type="checkbox" id="active"
                                               name="active"
                                               value={user.active} checked={user.active} onChange={handleChecked}/>
                                        <label className="form-check-label" htmlFor="active">Active</label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <input className="form-check-input" type="checkbox" id="meetingCompleted"
                                               name="meeting_completed"
                                            value={user.meeting_completed} checked={user.meeting_completed}
                                            onChange={handleChecked} />
                                        <label className="form-check-label" htmlFor="meetingCompleted">Meeting
                                            Completed</label>
                                    </div>
                                </div>
                            </div>
                        ) : null
                    }
                    <div className="form-group">
                        <input type="submit" className="form-control btn btn-primary col-sm col-md-3"
                               value="Update User"/>
                    </div>
                </form>
            </Fragment>
        );
};

const mapStateToProps = (state) => {
    return {
        user: state.GetUserReducer.user,
        updated: state.UpdateUserReducer.updated
    }
}

const UpdateUser = connect(mapStateToProps)(ConnectedUpdateUser)

// export default PaymentItem
UpdateUser.propTypes = {
    user: PropTypes.object.isRequired,
};

export default UpdateUser;
