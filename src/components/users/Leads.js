import React, {useEffect, Fragment} from 'react';
import {connect, useDispatch} from "react-redux";
import leadList from "../../redux/actions/LeadsListAction";
import UserCard from './UserCard'

const ConnectLeads = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(leadList())
        }, []
    );

    if (!props.error) {
        return (
            <Fragment>
                <h2>Leads</h2>
                <div className="row">
                    {props.leads && props.leads.map(el => (
                        <UserCard key={el.id} user={el}/>
                    ))}
                </div>
            </Fragment>
        )
    } else if (props.error === 404) {
        return (
            <Fragment>
                <h1>Error: {props.error}</h1>
                <h2>Oops... looks like there was nothing found for Leads!</h2>
            </Fragment>
        )
    } else
        return (
            <Fragment>
                <h1>Error! Please try again.</h1>
            </Fragment>
        )
}

const mapStateToProps = (state) => {
    return {
        leads: state.LeadListReducer.leads,
        error: state.LeadListReducer.error
    }
}

const Leads = connect(mapStateToProps)(ConnectLeads);

export default Leads;