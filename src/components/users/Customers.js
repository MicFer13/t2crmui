import React, {useEffect, Fragment} from 'react';
import {connect, useDispatch} from "react-redux";
import customerList from "../../redux/actions/CustomersListAction";
import UserCard from './UserCard'

const ConnectCustomers = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(customerList())
        }, []
    );

    if (!props.error) {
        return (
            <Fragment>
                <h2>Customers</h2>
                <div className="row">
                    {props.customers && props.customers.map(el => (
                        <UserCard key={el.id} user={el}/>
                    ))}
                </div>
            </Fragment>
        )
    } else if (props.error === 404) {
        return (
            <Fragment>
                <h1>Error: {props.error}</h1>
                <h2>Oops... looks like there was nothing found for Customers!</h2>
            </Fragment>
        )
    } else
        return (
            <Fragment>
                <h1>Error! Please try again.</h1>
            </Fragment>
        )
}

const mapStateToProps = (state) => {
    return {
        customers: state.CustomerListReducer.customers,
        error: state.CustomerListReducer.error
    }
}

const Customers = connect(mapStateToProps)(ConnectCustomers);

export default Customers;