import React, {useEffect, useState} from 'react'
import { Link } from "react-router-dom";

const UserCard = (props) => {
    const [user, setUser] = useState(props.user)

    useEffect(() => {
        setUser(props.user)
    }, [props])

    return (
        <div className="col-sm col-md-4 mb-3">
            <div className="card">
                <img src={require("../../images/" + user.id + ".jpg")}
                     className="card-img-top img-card-size d-none d-lg-block" alt=""/>
                <div className="card-body">
                    <h5 className="card-title">{user.firstname} {user.surname}</h5>
                    <hr/>
                    <li className="card-text">
                        Added Date: {user.initial_date}
                    </li>
                    <li className="card-text">
                        Gender: {user.gender}
                    </li>
                    <li className="card-text">
                        Email: {user.email_address}
                    </li>
                    <li className="card-text">
                        Phone: {user.phone_number}
                    </li>
                    <li className="card-text">
                        Emailed: {user.email_sent ? 'Yes' : 'No'}
                    </li>
                    <li className="card-text">
                        Called: {user.phone_call_made ? 'Yes' : 'No'}
                    </li>
                    <hr/>
                    <Link to={`/manager/${user.id}`} className="btn btn-primary">View</Link>
                    <Link to={`/history/${user.id}`} className="btn btn-primary float-right">History</Link>
                </div>
            </div>
        </div>
    )
}

export default UserCard;