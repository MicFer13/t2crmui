import React, {useEffect, Fragment} from 'react';
import {connect, useDispatch} from "react-redux";
import prospectList from "../../redux/actions/ProspectsListAction";
import UserCard from './UserCard'

const ConnectProspects = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(prospectList())
        }, []
    );

    if (!props.error) {
        return (
            <Fragment>
                <h2>Prospects</h2>
                <div className="row">
                    {props.prospects && props.prospects.map(el => (
                        <UserCard key={el.id} user={el}/>
                    ))}
                </div>
            </Fragment>
        )
    } else if (props.error === 404) {
        return (
            <Fragment>
                <h1>Error: {props.error}</h1>
                <h2>Oops... looks like there was nothing found for Prospects!</h2>
            </Fragment>
        )
    } else
        return (
            <Fragment>
                <h1>Error! Please try again.</h1>
            </Fragment>
        )
}

const mapStateToProps = (state) => {
    return {
        prospects: state.ProspectListReducer.prospects,
        error: state.ProspectListReducer.error
    }
}

const Prospects = connect(mapStateToProps)(ConnectProspects);

export default Prospects;