import React, { Fragment } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css'
import { Switch, Route } from "react-router-dom";
import Home from "./components/layout/Home";
import Manager from "./components/pages/Manager"
import History from "./components/pages/History";
import NavBar from './components/layout/Navbar'
import AddUser from "./components/users/AddUser";
import Leads from "./components/users/Leads";
import Prospects from "./components/users/Prospects";
import Customers from "./components/users/Customers";
import UpdateUser from "./components/users/UpdateUser";

const App = () => {
  return (
      <Fragment>
        <NavBar />
        <Route exact path="/" component={Home} />
        <section className="container mt-3">
            <Switch>
                <Route exact path="/leads" component={Leads} />
                <Route exact path="/prospects" component={Prospects} />
                <Route exact path="/customers" component={Customers} />
                <Route exact path="/adduser" component={AddUser} />
                <Route path="/manager/:id" component={Manager}/>
                <Route path="/history/:id" component={History}/>
                <Route path="/updateuser/:id" component={UpdateUser}/>
            </Switch>
        </section>
      </Fragment>
  );
}

export default App;
