import http from '../http-common';

export class UserService {
    getUsers(status) {
        return http.get(`/status?status=${status}`);
    }

    get(id) {
        return http.get(`/id?id=${id}`);
    }

    getHistory(id) {
        return http.get(`/logsparse?id=${id}`);
    }

    create(data) {
        return http.post("/adduser", data)
    }

    updateUser(user) {
        return http.post("/updateuser", user);
    }
}

export default new UserService();