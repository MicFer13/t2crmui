pipeline {
    agent any

    stages {
        stage ('Dependencies') {
            steps {
                println 'Installing dependencies'
                sh 'npm install'
                println 'All dependencies were installed-updated'
            }
        }
        stage ('Build') {
            steps {
                println 'Starting build'
                sh 'npm run-script build'
                println 'Completed the build'
            }
        }
        stage ('Delivery') {
            steps {
                fileOperation([fileCopyOperation(
                        excludes: '',
                        flattenFiles: false,
                        includes: 'build\\**',
                        targetLocation: 'C:\\MainProject\\frontend\\t2crmui\\crm-ui-app\\out'
                )])
            }
        }
    }
}